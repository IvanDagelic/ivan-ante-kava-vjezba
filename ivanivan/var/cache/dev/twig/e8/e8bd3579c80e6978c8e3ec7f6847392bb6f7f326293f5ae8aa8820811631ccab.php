<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_37df66717470cf83f3cdf5917c3c28f3a9e3f57be5e4a119f13386c488ea5993 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_36ca51d79dfecaf13b5cb0dd31b931596aa716a18bf548938f4bab4fc54a7747 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36ca51d79dfecaf13b5cb0dd31b931596aa716a18bf548938f4bab4fc54a7747->enter($__internal_36ca51d79dfecaf13b5cb0dd31b931596aa716a18bf548938f4bab4fc54a7747_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_de569b86adfa27f18d20cd9e28785d837dcbe21b773bcc6ba0d955649d99a693 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de569b86adfa27f18d20cd9e28785d837dcbe21b773bcc6ba0d955649d99a693->enter($__internal_de569b86adfa27f18d20cd9e28785d837dcbe21b773bcc6ba0d955649d99a693_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_36ca51d79dfecaf13b5cb0dd31b931596aa716a18bf548938f4bab4fc54a7747->leave($__internal_36ca51d79dfecaf13b5cb0dd31b931596aa716a18bf548938f4bab4fc54a7747_prof);

        
        $__internal_de569b86adfa27f18d20cd9e28785d837dcbe21b773bcc6ba0d955649d99a693->leave($__internal_de569b86adfa27f18d20cd9e28785d837dcbe21b773bcc6ba0d955649d99a693_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/var/www/ivan/ivanivan/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
