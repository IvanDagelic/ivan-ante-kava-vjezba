<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_70611e36ef8af73fbdfb2e9a4ba9b0c9a85964ec781a1189ec87c16cf8963b29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b035c4ddfb7859dea7405c4b6929fb03fe230823355f4a2b58e3b28f3a1f357 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b035c4ddfb7859dea7405c4b6929fb03fe230823355f4a2b58e3b28f3a1f357->enter($__internal_1b035c4ddfb7859dea7405c4b6929fb03fe230823355f4a2b58e3b28f3a1f357_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_fa30f7c692d4b580b229d80f6d9f22b72f01dba1a28f43e97d9ad02ed1b63daf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa30f7c692d4b580b229d80f6d9f22b72f01dba1a28f43e97d9ad02ed1b63daf->enter($__internal_fa30f7c692d4b580b229d80f6d9f22b72f01dba1a28f43e97d9ad02ed1b63daf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1b035c4ddfb7859dea7405c4b6929fb03fe230823355f4a2b58e3b28f3a1f357->leave($__internal_1b035c4ddfb7859dea7405c4b6929fb03fe230823355f4a2b58e3b28f3a1f357_prof);

        
        $__internal_fa30f7c692d4b580b229d80f6d9f22b72f01dba1a28f43e97d9ad02ed1b63daf->leave($__internal_fa30f7c692d4b580b229d80f6d9f22b72f01dba1a28f43e97d9ad02ed1b63daf_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6027536bf1e7c90a31e58b2cd45b51299c6bf0fcd206b89188b0d98bd36f5ac2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6027536bf1e7c90a31e58b2cd45b51299c6bf0fcd206b89188b0d98bd36f5ac2->enter($__internal_6027536bf1e7c90a31e58b2cd45b51299c6bf0fcd206b89188b0d98bd36f5ac2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_93487aa69005d8d690a3007025a1554c05699ab8e3d6d437b931c3a2e66a2b87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93487aa69005d8d690a3007025a1554c05699ab8e3d6d437b931c3a2e66a2b87->enter($__internal_93487aa69005d8d690a3007025a1554c05699ab8e3d6d437b931c3a2e66a2b87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_93487aa69005d8d690a3007025a1554c05699ab8e3d6d437b931c3a2e66a2b87->leave($__internal_93487aa69005d8d690a3007025a1554c05699ab8e3d6d437b931c3a2e66a2b87_prof);

        
        $__internal_6027536bf1e7c90a31e58b2cd45b51299c6bf0fcd206b89188b0d98bd36f5ac2->leave($__internal_6027536bf1e7c90a31e58b2cd45b51299c6bf0fcd206b89188b0d98bd36f5ac2_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_87c3860623e493db9c58a85b44f1b046aaf56eb2d9faed98d406ee6160ab049c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87c3860623e493db9c58a85b44f1b046aaf56eb2d9faed98d406ee6160ab049c->enter($__internal_87c3860623e493db9c58a85b44f1b046aaf56eb2d9faed98d406ee6160ab049c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c60654a25b01a982c5a9b45fb209aba70ee8c29b9832d6f09fc16fb37cfe49cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c60654a25b01a982c5a9b45fb209aba70ee8c29b9832d6f09fc16fb37cfe49cd->enter($__internal_c60654a25b01a982c5a9b45fb209aba70ee8c29b9832d6f09fc16fb37cfe49cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_c60654a25b01a982c5a9b45fb209aba70ee8c29b9832d6f09fc16fb37cfe49cd->leave($__internal_c60654a25b01a982c5a9b45fb209aba70ee8c29b9832d6f09fc16fb37cfe49cd_prof);

        
        $__internal_87c3860623e493db9c58a85b44f1b046aaf56eb2d9faed98d406ee6160ab049c->leave($__internal_87c3860623e493db9c58a85b44f1b046aaf56eb2d9faed98d406ee6160ab049c_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_e79ac0aa590ee39df47f7d6ebe8ba0705ad92e5769f67a29417751cca63ca193 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e79ac0aa590ee39df47f7d6ebe8ba0705ad92e5769f67a29417751cca63ca193->enter($__internal_e79ac0aa590ee39df47f7d6ebe8ba0705ad92e5769f67a29417751cca63ca193_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1b1a352494ea9c1a315ce0cf974094aacca7d84a9ce4128f6ac7fc5a9c0ba551 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b1a352494ea9c1a315ce0cf974094aacca7d84a9ce4128f6ac7fc5a9c0ba551->enter($__internal_1b1a352494ea9c1a315ce0cf974094aacca7d84a9ce4128f6ac7fc5a9c0ba551_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_1b1a352494ea9c1a315ce0cf974094aacca7d84a9ce4128f6ac7fc5a9c0ba551->leave($__internal_1b1a352494ea9c1a315ce0cf974094aacca7d84a9ce4128f6ac7fc5a9c0ba551_prof);

        
        $__internal_e79ac0aa590ee39df47f7d6ebe8ba0705ad92e5769f67a29417751cca63ca193->leave($__internal_e79ac0aa590ee39df47f7d6ebe8ba0705ad92e5769f67a29417751cca63ca193_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/ivan/ivanivan/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
