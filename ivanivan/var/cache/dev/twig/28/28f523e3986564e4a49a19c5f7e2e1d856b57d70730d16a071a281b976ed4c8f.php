<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b390fd88ae5b2e7c2545545e6a46f5954eb0f4397f65c77bded42c013e54790a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f49c7c1fd36b50c2cfe95049f251bee57e4c0bc6e96f5d50b0adc61ef76162b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f49c7c1fd36b50c2cfe95049f251bee57e4c0bc6e96f5d50b0adc61ef76162b->enter($__internal_2f49c7c1fd36b50c2cfe95049f251bee57e4c0bc6e96f5d50b0adc61ef76162b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_18212fb562db46d118189c96c4cb92f0512a7409debcd7347779755728d7c611 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18212fb562db46d118189c96c4cb92f0512a7409debcd7347779755728d7c611->enter($__internal_18212fb562db46d118189c96c4cb92f0512a7409debcd7347779755728d7c611_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f49c7c1fd36b50c2cfe95049f251bee57e4c0bc6e96f5d50b0adc61ef76162b->leave($__internal_2f49c7c1fd36b50c2cfe95049f251bee57e4c0bc6e96f5d50b0adc61ef76162b_prof);

        
        $__internal_18212fb562db46d118189c96c4cb92f0512a7409debcd7347779755728d7c611->leave($__internal_18212fb562db46d118189c96c4cb92f0512a7409debcd7347779755728d7c611_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_45d775811f0fa6dcbea9f49555f57c87da397495d3d2084f4635d148802dd70e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45d775811f0fa6dcbea9f49555f57c87da397495d3d2084f4635d148802dd70e->enter($__internal_45d775811f0fa6dcbea9f49555f57c87da397495d3d2084f4635d148802dd70e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_4e784e5ffadda398e6f5d8b84baa94d01c852b0e05943645c3fed34a17dec32b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e784e5ffadda398e6f5d8b84baa94d01c852b0e05943645c3fed34a17dec32b->enter($__internal_4e784e5ffadda398e6f5d8b84baa94d01c852b0e05943645c3fed34a17dec32b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4e784e5ffadda398e6f5d8b84baa94d01c852b0e05943645c3fed34a17dec32b->leave($__internal_4e784e5ffadda398e6f5d8b84baa94d01c852b0e05943645c3fed34a17dec32b_prof);

        
        $__internal_45d775811f0fa6dcbea9f49555f57c87da397495d3d2084f4635d148802dd70e->leave($__internal_45d775811f0fa6dcbea9f49555f57c87da397495d3d2084f4635d148802dd70e_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_bf4857b91329ac2e5c6ee2b95684922b6c7299b43d3c4640f601b2896c163b05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf4857b91329ac2e5c6ee2b95684922b6c7299b43d3c4640f601b2896c163b05->enter($__internal_bf4857b91329ac2e5c6ee2b95684922b6c7299b43d3c4640f601b2896c163b05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2914a1a84c4a5d2c6638bb1164170ef7f11ebe89bf5b454b7ee45d4f04d9e913 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2914a1a84c4a5d2c6638bb1164170ef7f11ebe89bf5b454b7ee45d4f04d9e913->enter($__internal_2914a1a84c4a5d2c6638bb1164170ef7f11ebe89bf5b454b7ee45d4f04d9e913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_2914a1a84c4a5d2c6638bb1164170ef7f11ebe89bf5b454b7ee45d4f04d9e913->leave($__internal_2914a1a84c4a5d2c6638bb1164170ef7f11ebe89bf5b454b7ee45d4f04d9e913_prof);

        
        $__internal_bf4857b91329ac2e5c6ee2b95684922b6c7299b43d3c4640f601b2896c163b05->leave($__internal_bf4857b91329ac2e5c6ee2b95684922b6c7299b43d3c4640f601b2896c163b05_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_3ca1d97edfd80f9c90e4dc5c43c6edc308b08e159fb25b739ee00ec7d55acec1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ca1d97edfd80f9c90e4dc5c43c6edc308b08e159fb25b739ee00ec7d55acec1->enter($__internal_3ca1d97edfd80f9c90e4dc5c43c6edc308b08e159fb25b739ee00ec7d55acec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_60fee7d425f11f10e07e289c4ca8bf647bf6d80d5fef3cf44a6b1a8ec14f03d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60fee7d425f11f10e07e289c4ca8bf647bf6d80d5fef3cf44a6b1a8ec14f03d5->enter($__internal_60fee7d425f11f10e07e289c4ca8bf647bf6d80d5fef3cf44a6b1a8ec14f03d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_60fee7d425f11f10e07e289c4ca8bf647bf6d80d5fef3cf44a6b1a8ec14f03d5->leave($__internal_60fee7d425f11f10e07e289c4ca8bf647bf6d80d5fef3cf44a6b1a8ec14f03d5_prof);

        
        $__internal_3ca1d97edfd80f9c90e4dc5c43c6edc308b08e159fb25b739ee00ec7d55acec1->leave($__internal_3ca1d97edfd80f9c90e4dc5c43c6edc308b08e159fb25b739ee00ec7d55acec1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/ivan/ivanivan/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
