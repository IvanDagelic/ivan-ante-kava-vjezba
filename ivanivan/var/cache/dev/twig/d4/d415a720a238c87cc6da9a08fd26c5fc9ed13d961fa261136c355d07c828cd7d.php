<?php

/* lucky/number.html.twig */
class __TwigTemplate_f97ff7fecded0e720d17696c052441011668cb1caec2b6beccfea963c5bfa41a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86ef42afa79a519f57354498411159d13f5bddd511420f1f3c1afec24d66e17f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86ef42afa79a519f57354498411159d13f5bddd511420f1f3c1afec24d66e17f->enter($__internal_86ef42afa79a519f57354498411159d13f5bddd511420f1f3c1afec24d66e17f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "lucky/number.html.twig"));

        $__internal_58fe3649ad01435bdf26212e8f76efc3408c25672b11803a5355e12b9c63da98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58fe3649ad01435bdf26212e8f76efc3408c25672b11803a5355e12b9c63da98->enter($__internal_58fe3649ad01435bdf26212e8f76efc3408c25672b11803a5355e12b9c63da98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "lucky/number.html.twig"));

        // line 2
        echo "
<h1>Your lucky number is ";
        // line 3
        echo twig_escape_filter($this->env, ($context["number"] ?? $this->getContext($context, "number")), "html", null, true);
        echo "</h1>";
        
        $__internal_86ef42afa79a519f57354498411159d13f5bddd511420f1f3c1afec24d66e17f->leave($__internal_86ef42afa79a519f57354498411159d13f5bddd511420f1f3c1afec24d66e17f_prof);

        
        $__internal_58fe3649ad01435bdf26212e8f76efc3408c25672b11803a5355e12b9c63da98->leave($__internal_58fe3649ad01435bdf26212e8f76efc3408c25672b11803a5355e12b9c63da98_prof);

    }

    public function getTemplateName()
    {
        return "lucky/number.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/lucky/number.html.twig #}

<h1>Your lucky number is {{ number }}</h1>", "lucky/number.html.twig", "/var/www/ivan/ivanivan/app/Resources/views/lucky/number.html.twig");
    }
}
